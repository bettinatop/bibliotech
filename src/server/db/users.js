var records = [
    { email: 'john', password: 'doe' },
    { email: 'asd', password: 'asd' },
    { email: 'jane', password: 'doe' }
];

exports.exists = function(email, cb) {
  process.nextTick(function() {
    var exists = records.find((rec) => {
      return rec.email === email;
    });
    console.log('exists?', exists);
    if (!exists) {
      cb(null, records[email]);
    } else {
      cb(new Error('User ' + email  + ' does not exist'));
    }
  });
}

exports.create = function(user, cb) {
  process.nextTick(function() {
    console.log('create', records)
    records.push({email: user.email, password: user.password});
    cb(null);
  });
}

exports.findByUsername = function(email, pass, cb) {
  process.nextTick(function() {
    for (var i = 0, len = records.length; i < len; i++) {
      var record = records[i];
      if (record.email === email && record.password === pass) {
        return cb(null, record);
      }
    }
    return cb(null, null);
  });
}