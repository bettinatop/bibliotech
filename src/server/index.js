
const express = require('express');
const session = require('express-session')
const passport = require('passport');
const Strategy = require('passport-local').Strategy;
const db = require('./db');
const bodyParser = require('body-parser');


passport.use(new Strategy( {
    usernameField: 'email',
    passwordField: 'password'
  }, (username, password, cb) => {
	console.log('local strategy', username, password)
    db.users.findByUsername(username, password, (err, user) => {
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    });
}));

passport.serializeUser(function(user, cb) {
	console.log('serializeUser', user);
  cb(null, user.email);
});

passport.deserializeUser(function(email, cb) {
	console.log('deserializeUser', user);
	console.log(user);
  db.users.findByUsername(email, password, function (err, user) {
    if (err) { return cb(err); }
    cb(null, user);
  });
});

const app = express();
app.use(express.static(__dirname + './../../'));

app.use(passport.initialize());

app.use(passport.session());

app.use(bodyParser.json());

app.post('/users/signup', (req, res) => {

	console.log('/users/signup', req.body);
	var data = req.body;
	db.users.exists(data.email, (err, user) => {
		if (err) {
			return res.send({error: 'User with '+ data.email +' email address already exists'});
		}

		db.users.create(data, (err) => {
			console.log(JSON.stringify(req.sessionID));
			return res.send({'user': data.email});
		});
	})
});

app.post('/users/signin', passport.authenticate('local', { failureRedirect: '/login' }), (req, res) => {

	console.log('Authenticated', req.body);
	var data = req.body;
	res.send({'user': data.email});
});

app.get('/login', (req, res) => {
    console.log('/login')
    res.redirect('/')
})

app.get('*', function(req, res) {
    console.log('*')
    res.sendfile('./index.html', {root: '.'});
});

app.listen(3000, () =>
	console.log('Example app listening on port 3000!'))
