# Bibliotech 

### Installation
Requires [Node.js](https://node.js.org) v6+ to run.

#### Run app
- Clone or download the repository 
- navigate to bibliotech folder 
```sh
cd bibliotech
npm start 
```

### Requirements

* Sign up - Login page with Polymer [DONE]
* Host Polymer in an Node.js express server [DONE]
* Save in a DB model users {installation problem with Mongo :/}[PARTIALLY DONE]
* Store sessions for logged in user [UNTOUCHED]
* GET /users [UNTOUCHED]
* Tests [UNTOUCHED]

